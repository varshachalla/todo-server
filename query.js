const graphql = require('graphql');

const TodoItemType = require('./types/todoItemType');
const TodoItem = require('./todoItem');

const {
    GraphQLID,
    GraphQLList,
    GraphQLObjectType,
} = graphql;

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        todoItem: {
            type: TodoItemType,
            args: { id: { type: GraphQLID } },
            resolve(args) {
                return TodoItem.findById(args.id);
            }
        },
        todoItems: {
            type: new GraphQLList(TodoItemType),
            resolve() {
                return TodoItem.find();
            }
        },
    }
});

module.exports = RootQuery;
