const { GraphQLID, GraphQLObjectType, GraphQLString } = require('graphql');

module.exports = new GraphQLObjectType({
    name: 'TodoItem',
    description: 'TodoItem',
    fields: () => ({
        id: { type: GraphQLID },
        body: { type: GraphQLString },
    })
});