const graphql = require('graphql');

const TodoItem = require('./todoItem');
const TodoItemType = require('./types/todoItemType');

const {
    GraphQLID,
    GraphQLObjectType,
    GraphQLString,
} = graphql;

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addTodoItem: {
            type: TodoItemType,
            args: {
                body: { type: GraphQLString },
            },
            resolve(parent, args) {
                let todoItem = new TodoItem({
                    body: args.body,
                });
                return todoItem.save();
            }
        },
        updateTodoItem: {
            type: TodoItemType,
            args: {
                body: { type: GraphQLString },
                id: { type: GraphQLID }
            },
            resolve(parent, args) {

                return TodoItem
                    .findOneAndUpdate(
                        {
                            _id: args.id
                        },
                        {
                            body: args.body
                        },
                        {
                            new: true,
                            runValidators: true
                        }).catch(err => {
                            console.error(err)
                        })
            }
        },
        deleteTodoItem: {
            type: TodoItemType,
            args: {
                id: { type: GraphQLID }
            },
            resolve(parent, args) {

                return TodoItem
                    .findOneAndRemove(
                        {
                            _id: args.id
                        },
                    ).catch(err => {
                        console.error(err)
                    })
            }
        }
    }
});

module.exports = Mutation;