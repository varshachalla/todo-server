var express = require("express");
const graphqlHTTP = require("express-graphql");
const graphql = require("graphql");

var Mutation = require("./mutation");
var RootQuery = require("./query");

const app = express();
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/todoDb");
mongoose.connection.once("open", () => {
  console.log("conneted to database");
});

const { GraphQLSchema } = graphql;

app.use(
  "/graphql",
  graphqlHTTP({
    schema: new GraphQLSchema({
      query: RootQuery,
      mutation: Mutation
    }),
    graphiql: true
  })
);

let port = 5000;
app.listen(port, () => {
  console.log("Server is up and running on port numner " + port);
});
